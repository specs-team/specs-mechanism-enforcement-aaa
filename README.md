# README #

OAUTH SERVER

The oauth server is developed using the spring framework.
It exposes three main APIs:

 /specs-oauth2-server/oauth/authorize
This API is useful to allow the user authorization. In particular this endpoint manage two phases: 
1) When it is called the first time it redirect the user to login page (for the authentication process)
2) When it is called subsequently it redirect the user to the authorization page

The API returns an univocal CODE at the “redirect_uri” to allow the client application to get the token.

TYPE OF REQUEST: GET
 
INPUT PARAMS:
"client_id" - URL PARAM
"redirect_uri" - URL PARAM
"response_type" - URL PARAM
"state" - URL PARAM

/specs-oauth2-server/oauth/token 
This API is useful to request the token that the client application can uses to get the end-user resources (the user profile in our case).

TYPE OF REQUEST: POST

INPUT PARAMS:
“grant_type” - BODY PARAM
"redirect_uri" - BODY PARAM
“code” - BODY PARAM

“client_id” - AUTHORIZATION HEADER - Basic 64
“client-secret” - AUTHORIZATION HEADER - Basic 64

/specs-oauth2-server/me
This API is useful to request the user profile (protected resource) using the token.

TYPE OF REQUEST: GET

INPUT PARAMS:
“token” - AUTHORIZATION HEADER - bearer


Flow Exploitation

1) A client application have to register on the oauth server (at the moment this feature is not public and a client application is statically registered into the server).

2) An end user have to register on the oath server (at the moment this feature is not public and a client application is statically registered into the server)

3) Client application has a button “Login with SPECS”. On the button click the  /specs-oauth2-server/oauth/authorize is called and the end-user is redirect on the oauth server, in particular to the login page (into the request the client application send a redirect uri)

4) The End-User insert his username and password

5) The End-User accept or deny the authorization to read his profile

6) The Oauth server call the “redirect uri”  on the client application passing an univocal code

7) The client application receives the code, so the end-user is correctly logged in. The client application use the code to call the api at /specs-oauth2-server/oauth/token to get the token value

8) The client application can read the user profile using the /specs-oauth2-server/me and the token value

CLIENT PARAMS
CLIENT-ID:  SPECS-Platform-Interface-App
CLIENT-SECRET: end-user-app-secret

END-USER PARAMS
USERNAME: specs-user-1
PASSWORD: specs-password

OWNER PARAMS
USERNAME: specs-owner
PASSWORD: specs-password

ADMIN PARAMS
USERNAME: manager
PASSWORD: pass