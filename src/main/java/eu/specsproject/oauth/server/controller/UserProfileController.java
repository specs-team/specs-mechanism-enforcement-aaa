package eu.specsproject.oauth.server.controller;

import java.security.Principal;
import java.util.Collection;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import eu.specsproject.oauth.server.entity.UserProfile;

/**
 * @author Michael Lavelle
 * 
 * Added to provide an endpoint from which Spring Social can obtain authentication details
 */
@RequestMapping("/me")
@Controller
public class UserProfileController {
	
	@ResponseBody
	@RequestMapping("")
	public UserProfile getProfileServiceUser(Principal principal)
	{
		Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>)    SecurityContextHolder.getContext().getAuthentication().getAuthorities();

		String role = "ROLE_USER";
		for (SimpleGrantedAuthority elem : authorities) {
	        role = elem.getAuthority();
	    }
		return new UserProfile(principal.getName(),principal.getName(),role);
	}
}
