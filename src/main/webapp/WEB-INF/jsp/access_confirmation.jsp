<%@ page
	import="org.springframework.security.core.AuthenticationException"%>
<%@ page
	import="org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter"%>
<%@ page
	import="org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException"%>
<%@ taglib prefix="authz"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Specs Oauth2 Server</title>
<link href="../bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>

<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
	padding-left: 50px;
	padding-right: 50px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>
</head>

<body>

	<jsp:include page="../../utils/navigator.jsp">
		<jsp:param name="_authorization" value="class='active'" />
		<jsp:param name="_img_path" value="src='../img/specs-logo-32.png'" />
	</jsp:include>

	<div class="tab-pane" id="tabStoreParams">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h2>SPECS Authorization</h2>
			</div>

			<div class="panel-body ">

				<sec:authorize access="isAuthenticated()">

					<div class="well">
						<div>
							Want you authorize the Application " <b><c:out
									value="${client.clientId}" /></b> " to access your protected
							resources?
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-body">
							<form id="confirmationForm" name="confirmationForm"
								action="<%=request.getContextPath()%>/oauth/authorize"
								method="post">
								<input name="user_oauth_approval" value="true" type="hidden" />
								<ul class="list-unstyled">
									<c:forEach items="${scopes}" var="scope">
										<c:set var="approved">
											<c:if test="${scope.value}"> checked</c:if>
										</c:set>
										<c:set var="denied">
											<c:if test="${!scope.value}"> checked</c:if>
										</c:set>
										<li>
											<div class="form-group">
												The ${client.clientId} application wants to ${scope.key}
												your resources. <br /> <input type="radio"
													name="${scope.key}" value="true" ${approved}>
												Approve</input> <br /> <input type="radio" name="${scope.key}"
													value="false" ${denied}> Deny</input>
											</div>
										</li>
									</c:forEach>
								</ul>
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
								<button class="btn btn-primary" type="submit">Submit</button>
							</form>
						</div>
					</div>
				</sec:authorize>

				<div class="panel panel-default">
					<div class="panel-body">
						<%
							if (session
									.getAttribute(AbstractAuthenticationProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY) != null
									&& !(session
											.getAttribute(AbstractAuthenticationProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY) instanceof UnapprovedClientAuthenticationException)) {
						%>
						<div class="error">
							<h2>Woops!</h2>

							<p>
								Access could not be granted. (<%=((AuthenticationException) session
						.getAttribute(AbstractAuthenticationProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY))
						.getMessage()%>)
							</p>
						</div>
						<%
							}
						%>
						<c:remove scope="session" var="SPRING_SECURITY_LAST_EXCEPTION" />


					</div>
				</div>

			</div>
		</div>
	</div>



	<jsp:include page="../../utils/footer.jsp" />

</body>
</html>
