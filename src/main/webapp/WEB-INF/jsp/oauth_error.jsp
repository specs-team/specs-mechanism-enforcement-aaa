<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Specs Oauth2 Server</title>
<link href="../bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>

<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
	padding-left: 50px;
	padding-right: 50px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>
</head>

<body>

	<jsp:include page="../../utils/navigator.jsp">
		<jsp:param name="_img_path" value="src='../img/specs-logo-32.png'" />
	</jsp:include>

	<div class="tab-pane" id="tabStoreParams">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h2>SPECS Oauth2 Error</h2>
			</div>

			<div class="panel-body ">
				<div class="well">
					<p>
						<c:out value="${message}" />
						(
						<c:out value="${error.summary}" />
						)
					</p>
					<p>Please go back to your client application and try again, or
						contact the owner and ask for support</p>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="../../utils/footer.jsp" />

</body>
</html>
