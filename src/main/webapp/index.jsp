<%@ taglib prefix="authz"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Specs Oauth2 Server</title>
<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
	padding-left: 50px;
	padding-right: 50px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

<authz:authorize ifAllGranted="ROLE_USER">
	<script type='text/javascript'>
		function pictureDisplay(json) {
			for (var i = 0; i < json.photos.length; i++) {
				var photo = json.photos[i];
				document
						.write('<img src="photos/' + photo.id + '" alt="' + photo.name + '">');
			}
		}
	</script>
</authz:authorize>
</head>
<body>

	<jsp:include page="utils/navigator.jsp">
		<jsp:param name="_home" value="class='active'" />
		<jsp:param name="_img_path" value="src='img/specs-logo-32.png'" />
	</jsp:include>

	<div class="tab-pane" id="tabStoreParams">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h2>SPECS Home</h2>
			</div>

			<div class="panel-body ">
				Welcome into SPECS home! <br /> This application allow you to
				manage your SPECS profile
			</div>

			<authz:authorize ifAllGranted="ROLE_USER">
				<div class="panel-body ">Your detected role is: USER</div>
			</authz:authorize>

			<authz:authorize ifAllGranted="ROLE_OWNER">
				<div class="panel-body ">Your detected role is: OWNER</div>
			</authz:authorize>
		</div>
	</div>
	
	<authz:authorize ifAllGranted="ROLE_USER">
		<div class="form-horizontal">
			<form action="<c:url value="/logout"/>" role="form" method="post">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" /><input type="hidden"
					name="redirect_url" value="<%= request.getParameter("redirect_url") %>" />
				<button class="btn btn-primary" type="submit">Logout</button>
			</form>
		</div>
	</authz:authorize>

	<authz:authorize ifAllGranted="ROLE_OWNER">
		<div class="form-horizontal">
			<form action="<c:url value="/logout"/>" role="form" method="post">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" /> <input type="hidden"
					name="redirect_url" value="<%= request.getParameter("redirect_url") %>" />
				<button class="btn btn-primary" type="submit">Logout</button>
			</form>
		</div>
	</authz:authorize>

	<jsp:include page="utils/footer.jsp" />

</body>
</html>
