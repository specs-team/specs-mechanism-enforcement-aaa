<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Specs Oauth2 Server</title>
<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
	padding-left: 50px;
	padding-right: 50px;
}

.sidebar-nav {
	padding: 9px 0;
}

.c {
	color: blue;
}
</style>
</head>

<body>

	<jsp:include page="utils/navigator.jsp">
		<jsp:param name="_login" value="class='active'" />
		<jsp:param name="_img_path" value="src='img/specs-logo-32.png'" />
	</jsp:include>

	<br />

	<div class="tab-pane" id="tabStoreParams">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h2>SPECS LogIn</h2>
			</div>

			<div class="panel-body ">
				<div class="form-horizontal">
					<p>Insert your SPECS username and password below and click
						SignIn button.</p>
					<form action="<c:url value="/login"/>" method="post" role="form">
						<fieldset>
							<legend>
								<h2>Login</h2>
							</legend>
							<div class="well">
								<div class="form-group">
									<label for="username">Username:</label> <input id="username"
										class="form-control" type='text' name='username' value="" />
								</div>
								<div class="form-group">
									<label for="password">Password:</label> <input id="password"
										class="form-control" type='password' name='password' value="" />
								</div>
								<button class="btn btn-primary" type="submit">SignIn</button>
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
							</div>
						</fieldset>
					</form>

				</div>

				<div class="panel panel-default">
					<div class="panel-body">
						<c:if test="${not empty param.authentication_error}">
							<font color="red">Authentication error: Username or
								Password are not correct</font>

						</c:if>
						<c:if test="${not empty param.authorization_error}">
							<font color="red">Authorization error: You are not
								permitted to access that resource</font>

						</c:if>
					</div>
				</div>
			</div>

		</div>

	</div>

	<jsp:include page="utils/footer.jsp" />

</body>
</html>
